# coding: utf-8
from collections import defaultdict

from cloudant import Cloudant
from flask import Flask, request, jsonify, url_for, redirect, render_template
import atexit
import os
import json
import sqlite3
from flask_bootstrap import Bootstrap

app = Flask(__name__, static_url_path='')
Bootstrap(app)


def init_sql_lite_base():
    try:
        with sqlite3.connect(db_path) as conn:
            cursor = conn.cursor()
            cursor.execute("""
            CREATE
            TABLE
            IF
            NOT
            EXISTS
            moyenne(
                id
            INTEGER
            PRIMARY
            KEY AUTOINCREMENT
            UNIQUE,
            date VARCHAR(40),
            sensorid
            VARCHAR(20),
            moyenne
            FLOAT
            )
            """)
            cursor.execute("""
            CREATE
            TABLE
            IF
            NOT
            EXISTS
            mediane(
                id
            INTEGER
            PRIMARY
            KEY AUTOINCREMENT
            UNIQUE,
            date VARCHAR(40),
            sensorid
            VARCHAR(20),
            mediane
            FLOAT
            )
            """)
            # Save (commit) the changes
            conn.commit()
    except sqlite3.OperationalError:
        print("Erreur la table existe deja")


db_name = 'mydb'
client = None
db = None
conn = None
values = []
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
db_path = os.path.join(BASE_DIR, "data/cours_poc.db")
init_sql_lite_base()

if 'VCAP_SERVICES' in os.environ:
    vcap = json.loads(os.getenv('VCAP_SERVICES'))
    print('Found VCAP_SERVICES')
    if 'cloudantNoSQLDB' in vcap:
        creds = vcap['cloudantNoSQLDB'][0]['credentials']
        user = creds['username']
        password = creds['password']
        url = 'https://' + creds['host']
        client = Cloudant(user, password, url=url, connect=True)
        db = client.create_database(db_name, throw_on_exists=False)
elif "CLOUDANT_URL" in os.environ:
    client = Cloudant(os.environ['CLOUDANT_USERNAME'], os.environ['CLOUDANT_PASSWORD'], url=os.environ['CLOUDANT_URL'],
                      connect=True)
    db = client.create_database(db_name, throw_on_exists=False)
elif os.path.isfile('vcap-local.json'):
    with open('vcap-local.json') as f:
        vcap = json.load(f)
        print('Found local VCAP_SERVICES')
        creds = vcap['services']['cloudantNoSQLDB'][0]['credentials']
        user = creds['username']
        password = creds['password']
        url = 'https://' + creds['host']
        client = Cloudant(user, password, url=url, connect=True)
        db = client.create_database(db_name, throw_on_exists=False)

# On IBM Cloud Cloud Foundry, get the port number from the environment variable PORT
# When running this app on the local machine, default the port to 8000
port = int(os.getenv('PORT', 8000))


def get_all_sensors_id():
    with sqlite3.connect(db_path) as conn:
        cursor = conn.cursor()
        cursor.execute("""SELECT DISTINCT sensorid FROM moyenne ORDER BY id DESC""")
        rows = cursor.fetchall()
        res = []
        for row in rows:
            res.append(row[0])
    return res


def sensor_value_by_sensor_id(sensor_id, limit):
    resultats = defaultdict(lambda: defaultdict(dict))
    with sqlite3.connect(db_path) as conn:
        cursor = conn.cursor()
        cursor.execute("SELECT sensorid ,moyenne, date FROM moyenne WHERE sensorid = ? ORDER BY id DESC", [sensor_id])
        rows = cursor.fetchall()
        for row in rows:
            if len(resultats[row[0]]["moyenne"]) <= limit - 1:
                resultats[row[0]]["moyenne"][row[2]] = row[1]

        cursor.execute("SELECT sensorid ,mediane, date FROM mediane WHERE sensorid = ? ORDER BY id DESC", [sensor_id])
        rows = cursor.fetchall()
        for row in rows:
            if len(resultats[row[0]]["mediane"]) <= limit - 1:
                resultats[row[0]]["mediane"][row[2]] = row[1]

    return resultats


@app.route('/iot_status/<sensor_id>', methods=['GET'])
def iot_status(sensor_id):
    moyennes = dict(sensor_value_by_sensor_id(sensor_id, limit=5)[sensor_id]['moyenne'])
    medianes = dict(sensor_value_by_sensor_id(sensor_id, limit=5)[sensor_id]['mediane'])

    return render_template('iot_status.html', title="Analyse des capteurs", sensor_ids=get_all_sensors_id(),
                           sensor_id=sensor_id, medianes=medianes, moyennes=moyennes)


@app.route('/')
def root():
    return render_template('home.html', title="Analyse des capteurs", sensor_ids=get_all_sensors_id())


@atexit.register
def shutdown():
    if client:
        client.disconnect()
    if conn is not None:
        conn.close()


@app.route('/api/get_last_means/<sensor_id>', methods=['GET'])
def get_last_means(sensor_id):
    moyennes = dict(sensor_value_by_sensor_id(sensor_id, limit=5)[sensor_id]['moyenne'])
    return jsonify(moyennes)


@app.route('/api/get_last_medianes/<sensor_id>', methods=['GET'])
def get_last_medianes(sensor_id):
    medianes = dict(sensor_value_by_sensor_id(sensor_id, limit=5)[sensor_id]['mediane'])
    return jsonify(medianes)


@app.route('/api/sensorvalues', methods=['GET'])
def get_all_sensor_values():
    resultats = defaultdict(lambda: defaultdict(dict))
    limit = int(request.args.get('limit')) if request.args.get('limit') is not None else 10
    with sqlite3.connect(db_path) as conn:
        cursor = conn.cursor()
        cursor.execute("""SELECT sensorid ,moyenne, date FROM moyenne ORDER BY id DESC""")
        rows = cursor.fetchall()
        for row in rows:
            if len(resultats[row[0]]["moyenne"]) <= limit - 1:
                resultats[row[0]]["moyenne"][row[2]] = row[1]

        cursor.execute("""SELECT sensorid ,mediane, date FROM mediane ORDER BY id DESC""")
        rows = cursor.fetchall()
        for row in rows:
            if len(resultats[row[0]]["mediane"]) <= limit - 1:
                resultats[row[0]]["mediane"][row[2]] = row[1]

    return jsonify(resultats)


@app.route('/api/sensorvaluesforandroid', methods=['GET'])
def get_all_sensor_values_for_android():
    resultats = defaultdict(lambda: defaultdict(list))
    limit = int(request.args.get('limit')) if request.args.get('limit') is not None else 10
    with sqlite3.connect(db_path) as conn:
        cursor = conn.cursor()
        cursor.execute("""SELECT sensorid ,moyenne, date FROM moyenne ORDER BY id DESC""")
        rows = cursor.fetchall()
        for row in rows:
            if len(resultats[row[0]]["moyennes"]) <= limit - 1:
                resultats[row[0]]["moyennes"].append({"date": row[2], "moyenne": row[1]})

        cursor.execute("""SELECT sensorid ,mediane, date FROM mediane ORDER BY id DESC""")
        rows = cursor.fetchall()
        for row in rows:
            if len(resultats[row[0]]["medianes"]) <= limit - 1:
                resultats[row[0]]["medianes"].append({"date": row[2], "mediane": row[1]})

    return jsonify(resultats)


@app.route('/api/sensorvalue', methods=['GET'])
def get_sensor_value_by_sensor_id():
    limit = int(request.args.get('limit')) if request.args.get('limit') is not None else 10
    return jsonify(sensor_value_by_sensor_id(request.args.get('sensor_id'), limit=limit))


@app.route('/api/addvalues', methods=['POST'])
def add_values():
    with sqlite3.connect(db_path) as conn:
        cursor = conn.cursor()

        for item in request.json:
            cursor.execute("""
            INSERT INTO moyenne(sensorid, moyenne, date) VALUES(?, ?, ?)""",
                           (item["id"], item["moyenne"], item["date"]))
            cursor.execute("""
            INSERT INTO mediane(sensorid, mediane, date) VALUES(?, ?, ?)""",
                           (item["id"], item["mediane"], item["date"]))
            conn.commit()
    return jsonify(success=True)


@app.route('/api/addvalue', methods=['POST'])
def add_value():
    with sqlite3.connect(db_path) as conn:
        item = request.json
        cursor = conn.cursor()

        cursor.execute("""
            INSERT INTO moyenne(sensorid, moyenne, date) VALUES(?, ?, ?)""",
                       (item["id"], item["moyenne"], item["date"]))
        cursor.execute("""
            INSERT INTO mediane(sensorid, mediane,date) VALUES(?, ?, ?)""",
                       (item["id"], item["mediane"], item["date"]))
        conn.commit()

    return jsonify(success=True)


@app.route('/api/deleteallsensorvalues', methods=['GET'])
def delete_all_sensor_values():
    with sqlite3.connect(db_path) as conn:
        cursor = conn.cursor()
        cursor.execute("""DELETE FROM moyenne""")
        cursor.execute("""DELETE FROM mediane""")
        conn.commit
    return jsonify(success=True)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=port, debug=True)
